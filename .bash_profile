speedUpMac() {
    # Disable Dashboard Widgets
    defaults write com.apple.dashboard mcx-disabled -boolean true
    # Enable again:
    #defaults write com.apple.dashboard mcx-disabled -boolean false

    # Clear Out Font Caches
    # atsutil databases -remove # for all user
    atsutil databases -removeUser

    # Use the 2D Dock
    defaults write com.apple.dock no-glass -boolean true
    # Enable again:
    #defaults write com.apple.dock no-glass -boolean NO

    # Disable Automatic Window Animations
    defaults write NSGlobalDomain NSAutomaticWindowAnimationsEnabled -bool false
    # Enable again:
    #defaults delete NSGlobalDomain NSAutomaticWindowAnimationsEnabled

    # Speed Up Dialog Boxes
    defaults write NSGlobalDomain NSWindowResizeTime x.y
    # Enable again:
    #defaults write NSGlobalDomain NSWindowResizeTime

    # Disable animations when opening a Quick Look window.
    defaults write -g QLPanelAnimationDuration -float 0

    # Accelerated playback when adjusting the window size (Cocoa applications).
    defaults write NSGlobalDomain NSWindowResizeTime -float 0.001

    # Disable animation when opening the Info window in Finder (cmd⌘ + i).
    defaults write com.apple.dock launchanim -bool false

    # Make all animations faster that are used by Mission Control.
    defaults write com.apple.dock expose-animation-duration -float 0.1

    # Disable the delay when you hide the Dock
    defaults write com.apple.Dock autohide-delay -float 0

    # Disable the animation when you sending and replying an e-mail - MAIL
    defaults write com.apple.mail DisableReplyAnimations -bool true
    defaults write com.apple.mail DisableSendAnimations -bool true

    # Disable the standard delay in rendering a Web page. - SAFARI
    defaults write com.apple.Safari WebKitInitialTimedLayoutDelay 0.25

    # Disable auto restore feature
    defaults write com.apple.Preview NSQuitAlwaysKeepsWindows -bool false

    # Disable special characters when holding keys
    #defaults write NSGlobalDomain ApplePressAndHoldEnabled -bool false

    # Normal minimum is 15 (225 ms)
    #defaults write -g InitialKeyRepeat -float 10.0
    #defaults write NSGlobalDomain InitialKeyRepeat -float 10.0

    # Normal minimum is 2 (30 ms)
    #defaults write NSGlobalDomain KeyRepeat -float 1.0
    #defaults write -g KeyRepeat -float 1.0

    # Restart Dock.
    killall Dock;

    # Run maintenance scripts:
    sudo periodic daily weekly monthly
    ls -al /var/log/*.out

    # Remove all .asl Apple System Log files
    sudo rm -if /private/var/log/asl/*.asl

    # Clear terminal history
    history -c

    # Clear Chrome History
    echo -e "Chrome app dir: $CHROMEDIR"
    echo -e "Chrome cache dir: $CHROMECACHE"
    rm -rf "$CHROMEDIR"/History
    rm -rf "$CHROMEDIR"/History-journal
    rm -rf "$CHROMEDIR"/Cookies
    rm -rf "$CHROMECACHE"/Cache
    rm -rf "$CHROMECACHE"/Storage
    rm -rf "$CHROMECACHE"/Code\ Cache
    rm -rf "$CHROMECACHE"/Media\ Cache

    # Clear Trash
    #rm -rf ~/.Trash/*

    # Clear Downloads directory
    #rm -rf $HOME/Downloads/*

    # Clear cache directory
    rm -rf $HOME/Library/Caches/

    # Purge memory
    sudo purge

    # Delete some custom contents:
    rm $HOME/java_error_in_phpstorm.hprof

    # Disable Adobe creative cloud auto startup.
    launchctl unload -w /Library/LaunchAgents/com.adobe.AdobeCreativeCloud.plist
}   